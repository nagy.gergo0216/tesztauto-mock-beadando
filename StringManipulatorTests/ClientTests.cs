﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MockPractice;
using Moq;
using NUnit.Framework;

namespace StringManipulatorTests
{
    [TestFixture]
    public class ClientTests
    {
        private Mock<IContentFormatter> _mockedContentFormatter;
        private Mock<IService> _mockedService;
        private Client _client;

        [OneTimeSetUp]
        public void Setup()
        {
            _mockedContentFormatter=new Mock<IContentFormatter>();
            _mockedService=new Mock<IService>();
            _mockedService.Setup(x => x.Name).Returns("mockService");

            _client=new Client(_mockedService.Object,_mockedContentFormatter.Object);
        }

        [Test]
        public void GetIdentity_ShouldReturnInputAsString_WhenInvoked()
        {
            var result = _client.GetIdentity();

            Assert.AreEqual("2",result);
        }

        [Test]
        public void GetIdentityFormatted_ShouldReturnFormattedIdentity_WhenInvoked()
        {
            var result = _client.GetIdentityFormatted();

            Assert.That(result.StartsWith("<formatted>"));
            Assert.That(result.Contains(_client.GetIdentity()));
            Assert.That(result.EndsWith("</formatted>"));
        }

        public void GetServiceName_ShouldReturnNameOfService_WhenInvoked()
        {
            var result = _client.GetServiceName();

            Assert.AreEqual("mockService",result);
        }

        [Test]
        public void Dispose_ShouldInvokeIDisposableDispose_WhenInvoked()
        {
            _client.Dispose();

            _mockedService.Verify(x => x.Dispose(), Times.Once());

        }


        [TestCase(12)]
        [TestCase(-12)]
        [TestCase(long.MaxValue)]
        [TestCase(long.MinValue)]
        public void GetContentFormatted_ShouldReturnFormattedContent_WhenServiceIsConnected(long input)
        {
            _mockedService.SetupGet(x => x.IsConnected).Returns(true);
            _mockedService.Setup(x => x.GetContent(input)).Returns(input.ToString);
            _mockedContentFormatter.Setup(x => x.Format(input.ToString()))
                .Returns($"<formatted> {input.ToString()} </formatted>");

            var result = _client.GetContentFormatted(input);

            _mockedService.Verify(x=>x.Connect(),Times.Never);
            Assert.AreEqual($"<formatted> {input.ToString()} </formatted>",result);

        }


        [TestCase(12)]
        [TestCase(-12)]
        [TestCase(long.MaxValue)]
        [TestCase(long.MinValue)]
        public void GetContentFormatted_ShouldReturnFormattedContent_WhenServiceIsNotConnected(long input)
        {
            _mockedService.Setup(x => x.GetContent(input)).Returns(input.ToString); ;
            _mockedService.SetupGet(x => x.IsConnected).Returns(false);
            _mockedContentFormatter.Setup(x => x.Format(input.ToString()))
                .Returns($"<formatted> {input.ToString()} </formatted>");

            var result = _client.GetContentFormatted(input);

            _mockedService.Verify(x => x.Connect(), Times.AtLeastOnce);
            Assert.AreEqual($"<formatted> {input.ToString()} </formatted>", result);

        }

    }
}
