﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer.Server;
using MockPractice;
using NUnit.Framework;

namespace StringManipulatorTests
{
    [TestFixture]
    public class StringManipulatorTests
    {
        private StringManipulator _stringManipulator;

        [OneTimeSetUp]
        public void Setup()
        {
            _stringManipulator=new StringManipulator();
        }

        [Test]
        public void Transform_ShouldReturnInput_WhenInputIsCorrect()
        {
            var result = _stringManipulator.Transform("asd");

            Assert.AreEqual("asd",result);
        }


        [TestCase("   ")]
        [TestCase("")]
        public void Transform_ShouldReturnArgumentException_WhenInputIsNotCorrect(string input)
        {
            Assert.Throws(typeof(ArgumentException), () => _stringManipulator.Transform(input));
        }

    }
}
